package com.phile.yrj.tema3;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by usuario on 24/11/16.
 */

public class Noticia {
    private String titulo;
    private String descripcion;
    private String link;
    private String pubDate;


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    @Override
    public String toString() {
        return titulo;
    }

    public String getPubDateFormated() {
        String date;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
            date = dateFormat.format(Date.parse(pubDate));
        } catch (Exception ex) {
            date = pubDate;
        }
        return date;
    }
}
