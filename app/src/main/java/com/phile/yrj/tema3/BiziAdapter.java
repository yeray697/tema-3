package com.phile.yrj.tema3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by usuario on 28/11/16.
 */

public class BiziAdapter extends ArrayAdapter{
    Context context;
    public BiziAdapter(Context context, List objects) {
        super(context, R.layout.bizi_list_item, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.bizi_list_item,null);
            holder = new Holder();
            holder.tvEstacion = (TextView) view.findViewById(R.id.tvEstacion);
            holder.tvActualizado = (TextView) view.findViewById(R.id.tvActualizado);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        Bizi aux = (Bizi) getItem(position);
        holder.tvEstacion.setText(aux.getTitulo());
        holder.tvActualizado.setText(aux.getUltimaActualizacion());
        return view;
    }
    class Holder{
        TextView tvEstacion;
        TextView tvActualizado;
    }
}
