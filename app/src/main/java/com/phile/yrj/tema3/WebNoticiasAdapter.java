package com.phile.yrj.tema3;

        import android.content.Context;
        import android.media.Image;
        import android.net.Uri;
        import android.support.annotation.NonNull;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.squareup.picasso.Picasso;

        import java.util.List;

/**
 * Created by usuario on 28/11/16.
 */

public class WebNoticiasAdapter extends ArrayAdapter {
    Context context;
    public WebNoticiasAdapter(Context context, List objects) {
        super(context, R.layout.web_noticia_list_item, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.web_noticia_list_item,null);
            holder = new Holder();
            holder.ivWeb = (ImageView) view.findViewById(R.id.ivNoticia);
            holder.tvTitulo = (TextView) view.findViewById(R.id.tvTitleWeb);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        WebNoticia aux = (WebNoticia) getItem(position);
        Picasso.with(getContext())
                .load(aux.getImage())
                .into(holder.ivWeb);
        holder.tvTitulo.setText(aux.getTitulo());
        return view;
    }
    class Holder{
        ImageView ivWeb;
        TextView tvTitulo;
    }
}
