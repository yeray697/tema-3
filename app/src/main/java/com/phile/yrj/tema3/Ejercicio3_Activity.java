package com.phile.yrj.tema3;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ejercicio3_Activity extends AppCompatActivity {
    Utilities.Downloaded downloaded;

    ListView lvBizi;
    BiziAdapter adapter;

    private String url = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml";
    static final String TEMPORAL = "biziTemporal.xml";

    List<Bizi> noticias;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);
        lvBizi = (ListView) findViewById(R.id.lvBizi);
        noticias = new ArrayList<>();
        adapter = new BiziAdapter(this,noticias);
        lvBizi.setAdapter(adapter);
        downloaded = new Utilities.Downloaded() {
            @Override
            public void onDownloadedResult() {
                noticias =  Utilities.readXML3(Utilities.readExternal(TEMPORAL,"UTF-8"));
                adapter = new BiziAdapter(Ejercicio3_Activity.this,noticias);
                lvBizi.setAdapter(adapter);
            }

            @Override
            public void onFailureResult() {

            }
        };
        lvBizi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Ejercicio3_Activity.this, Ejercicio3_2_Activity.class);
                intent.putExtra("bizi", (Bizi) adapter.getItem(i));
                startActivity(intent);
            }
        });
        Utilities.descargarFichero(url, TEMPORAL, Ejercicio3_Activity.this, downloaded);
        ((Button)findViewById(R.id.btActualizar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.descargarFichero(url, TEMPORAL, Ejercicio3_Activity.this, downloaded);
            }
        });
    }
}
