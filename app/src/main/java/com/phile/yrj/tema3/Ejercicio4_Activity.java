package com.phile.yrj.tema3;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio4_Activity extends AppCompatActivity {

    ListView lvNews;
    WebNoticiasAdapter adapter;

    final String URL = "http://ep00.epimg.net/rss/elpais/portada.xml";
    static final String TEMPORAL = "noticiasTemporal.xml";

    List<WebNoticia> noticias;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);
        lvNews = (ListView) findViewById(R.id.lvNews4);
        noticias = new ArrayList<>();
        addWebs();
        adapter = new WebNoticiasAdapter(this,noticias);
        lvNews.setAdapter(adapter);
        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Ejercicio4_Activity.this, Ejercicio4_2_Activity.class);
                intent.putExtra("rss",((WebNoticia)adapter.getItem(i)).getRss());
                startActivity(intent);
            }
        });
    }

    private void addWebs() {
        noticias.add(new WebNoticia("https://pbs.twimg.com/profile_images/767707559755153408/Tpz87VyY.jpg","ABC","http://www.abc.es/rss/feeds/abc_EspanaEspana.xml"));
        noticias.add(new WebNoticia("https://pbs.twimg.com/profile_images/378800000384529554/ce1c0f07805fb6c778364d8ad578a349_normal.png","El jueves","http://viejuna.eljueves.es/feeds/rss.html"));
        noticias.add(new WebNoticia("https://static-s.aa-cdn.net/img/gp/20600002074002/UMtfMWHGxntDxcY-wP6kud0LsnocnASHcIt-uhaSmKE_Tea2HpKpTUXWaXRcOjtNw20=w300?v=1","El País","http://ep00.epimg.net/rss/elpais/portada.xml"));
        noticias.add(new WebNoticia("https://pbs.twimg.com/profile_images/625708881453445120/hKNzu6K-_reasonably_small.png","El Mundo Today","http://www.elmundotoday.com/feed/"));
    }
}
