package com.phile.yrj.tema3;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Ejercicio3_2_Activity extends AppCompatActivity {
    TextView tvTitulo, tvEstado, tvBicisDisp, tvAnclajeDisp,tvUltimaActBizi;
    Button btShow;
    Bizi bizi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3_2);
        bizi = (Bizi) getIntent().getExtras().getSerializable("bizi");
        tvTitulo = (TextView) findViewById(R.id.tvTituloBizi);
        tvEstado = (TextView) findViewById(R.id.tvEstadoBizi);
        tvBicisDisp = (TextView) findViewById(R.id.tvBicisDisp);
        tvAnclajeDisp = (TextView) findViewById(R.id.tvAnclajeDisp);
        tvUltimaActBizi = (TextView) findViewById(R.id.tvUltimaActBizi);
        btShow = (Button) findViewById(R.id.btShow);

        tvTitulo.setText(bizi.getTitulo());
        tvEstado.setText("Estado: " + bizi.getEstado());
        tvBicisDisp.setText("Bicis disponibles: " + bizi.getBicisDisponibles());
        tvAnclajeDisp.setText("Anclajes disponibles: " + bizi.getAnclajesDisponibles());
        tvUltimaActBizi.setText(bizi.getUltimaActualizacion());
        btShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(bizi.getUri());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
}
