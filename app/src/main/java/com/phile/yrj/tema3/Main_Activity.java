package com.phile.yrj.tema3;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main_Activity extends AppCompatActivity implements View.OnClickListener{

    Button btEjercicio1,btEjercicio2,btEjercicio3,btEjercicio4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_);
        btEjercicio1 = (Button) findViewById(R.id.btEjercicio1);
        btEjercicio2 = (Button) findViewById(R.id.btEjercicio2);
        btEjercicio3 = (Button) findViewById(R.id.btEjercicio3);
        btEjercicio4 = (Button) findViewById(R.id.btEjercicio4);

        btEjercicio1.setOnClickListener(this);
        btEjercicio2.setOnClickListener(this);
        btEjercicio3.setOnClickListener(this);
        btEjercicio4.setOnClickListener(this);

        ///Checking permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.INTERNET}, 2909);
            }
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btEjercicio1:
                intent = new Intent(Main_Activity.this,Ejercicio1_Activity.class);
                break;
            case R.id.btEjercicio2:
                intent = new Intent(Main_Activity.this,Ejercicio2_Activity.class);
                break;
            case R.id.btEjercicio3:
                intent = new Intent(Main_Activity.this,Ejercicio3_Activity.class);
                break;
            case R.id.btEjercicio4:
                intent = new Intent(Main_Activity.this,Ejercicio4_Activity.class);
                break;
        }
        startActivity(intent);
    }
}
