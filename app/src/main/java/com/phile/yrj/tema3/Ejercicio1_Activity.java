package com.phile.yrj.tema3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Ejercicio1_Activity extends AppCompatActivity {

    TextView tvNEmpleados,tvEdadMedia, tvSueldoMin, tvSueldoMax;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);
        tvNEmpleados = (TextView) findViewById(R.id.tvNEmpleados1);
        tvEdadMedia = (TextView) findViewById(R.id.tvEdadMedia1);
        tvSueldoMin = (TextView) findViewById(R.id.tvSueldoMin1);
        tvSueldoMax = (TextView) findViewById(R.id.tvSueldoMax1);

        List<Empleado> empleados = Utilities.readXML1(getResources().getXml(R.xml.empleados));
        float edadMedia = 0;
        float sueldoMin = Float.MAX_VALUE;
        float sueldoMax = Float.MIN_VALUE;
        float sueldo;

        for (Empleado empleado: empleados) {
            edadMedia += empleado.getEdad();
            sueldo = empleado.getSueldo();

            if (sueldo > sueldoMax)
                sueldoMax = sueldo;
            if (sueldo < sueldoMin)
                sueldoMin = sueldo;
        }
        edadMedia = edadMedia / empleados.size();

        tvNEmpleados.setText("Número de empleados en el fichero: "+ empleados.size());
        tvEdadMedia.setText("Edad media de los empleados del fichero: "+ String.format("%.2f", edadMedia) + " años");
        tvSueldoMax.setText("Sueldo máximo de los empleados: "+ sueldoMax + " euros");
        tvSueldoMin.setText("Sueldo mínimo de los empleados: "+ sueldoMin + " euros");
    }
}
