package com.phile.yrj.tema3;

/**
 * Created by yeray697 on 7/12/16.
 */
public class WebNoticia {

    String image;
    String titulo;

    public WebNoticia(String image, String titulo, String rss) {
        this.image = image;
        this.titulo = titulo;
        this.rss = rss;
    }

    public String getRss() {
        return rss;
    }

    public void setRss(String rss) {
        this.rss = rss;
    }

    String rss;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
