package com.phile.yrj.tema3;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by usuario on 28/11/16.
 */

public class NoticiasAdapter extends ArrayAdapter{
    Context context;
    public NoticiasAdapter(Context context, List objects) {
        super(context, R.layout.noticia_list_item, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.noticia_list_item,null);
            holder = new Holder();
            holder.tvTitulo = (TextView) view.findViewById(R.id.tvTitle);
            holder.tvFecha = (TextView) view.findViewById(R.id.tvFecha);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        Noticia aux = (Noticia) getItem(position);
        holder.tvFecha.setText(aux.getPubDateFormated());
        holder.tvTitulo.setText(aux.getTitulo());
        return view;
    }
    class Holder{
        TextView tvTitulo;
        TextView tvFecha;
    }
}
