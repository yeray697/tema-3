package com.phile.yrj.tema3;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio4_2_Activity extends AppCompatActivity {
    Utilities.Downloaded downloaded;

    ListView lvNews;
    NoticiasAdapter adapter;

    private String url = "";
    static final String TEMPORAL = "noticiasTemporal.xml";

    List<Noticia> noticias;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4_2);
        url = getIntent().getExtras().getString("rss","");
        lvNews = (ListView) findViewById(R.id.lvNews4_2);
        noticias = new ArrayList<>();
        adapter = new NoticiasAdapter(this,noticias);
        lvNews.setAdapter(adapter);
        downloaded = new Utilities.Downloaded() {
            @Override
            public void onDownloadedResult() {
                noticias =  Utilities.readXML4(Utilities.readExternal(TEMPORAL,"UTF-8"));
                adapter = new NoticiasAdapter(Ejercicio4_2_Activity.this,noticias);
                lvNews.setAdapter(adapter);
            }

            @Override
            public void onFailureResult() {
                finish();
            }
        };
        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = Uri.parse(noticias.get(i).getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        Utilities.descargarFichero(url, TEMPORAL, Ejercicio4_2_Activity.this, downloaded);
    }
}