package com.phile.yrj.tema3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Ejercicio2_Activity extends AppCompatActivity {

    TextView tvFechaToday, tvFechaTomorrow;
    ImageView ivEstado1Today,ivEstado2Today,ivEstado3Today, ivEstado4Today;
    ImageView ivEstado1Tomorrow,ivEstado2Tomorrow,ivEstado3Tomorrow, ivEstado4Tomorrow;
    TextView tvTemperaturaMaxToday, tvTemperaturaMaxTomorrow;
    TextView tvTemperaturaMinToday, tvTemperaturaMinTomorrow;
    Button btRefresh;
    Utilities.Downloaded downloaded;

    final String URL = "http://www.aemet.es/xml/municipios/localidad_29067.xml";
    static final String TEMPORAL = "tiempo.xml";
    List<TiempoDiario> tiempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);
        setTitle("El tiempo");
        downloaded = new Utilities.Downloaded() {
            @Override
            public void onDownloadedResult() {
                tiempo = Utilities.readXML2(Utilities.readExternal(TEMPORAL,"UTF-8"));
                Toast.makeText(Ejercicio2_Activity.this, "Fichero descargado correctamente", Toast.LENGTH_SHORT).show();
                tvFechaToday.setText(parseDate(tiempo.get(0).getDay()));
                tvFechaTomorrow.setText(parseDate(tiempo.get(1).getDay()));
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(0).getEstadoCielo00())
                        .into(ivEstado1Today);
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(0).getEstadoCielo06())
                        .into(ivEstado2Today);
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(0).getEstadoCielo12())
                        .into(ivEstado3Today);
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(0).getEstadoCielo18())
                        .into(ivEstado4Today);
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(1).getEstadoCielo00())
                        .into(ivEstado1Tomorrow);
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(1).getEstadoCielo06())
                        .into(ivEstado2Tomorrow);
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(1).getEstadoCielo12())
                        .into(ivEstado3Tomorrow);
                Picasso.with(getApplicationContext())
                        .load(tiempo.get(1).getEstadoCielo18())
                        .into(ivEstado4Tomorrow);
                //tvEstadoToday.setText(tiempo.get(0).getEstadoCielo());
                //tvEstadoTomorrow.setText(tiempo.get(1).getEstadoCielo());
                tvTemperaturaMaxToday.setText(String.valueOf(tiempo.get(0).getTemperaturaMax()) + "ºC");
                tvTemperaturaMaxTomorrow.setText(String.valueOf(tiempo.get(1).getTemperaturaMax()) + "ºC");
                tvTemperaturaMinToday.setText(String.valueOf(tiempo.get(0).getTemperaturaMin()) + "ºC");
                tvTemperaturaMinTomorrow.setText(String.valueOf(tiempo.get(1).getTemperaturaMin()) + "ºC");
            }

            @Override
            public void onFailureResult() {

            }
        };
        btRefresh = (Button) findViewById(R.id.btRefresh2);
        tvFechaToday = (TextView) findViewById(R.id.tvFechaToday);
        tvFechaTomorrow = (TextView) findViewById(R.id.tvFechaTomorrow);
        ivEstado1Today = (ImageView) findViewById(R.id.ivEstadoCielo11);
        ivEstado2Today = (ImageView) findViewById(R.id.ivEstadoCielo21);
        ivEstado3Today = (ImageView) findViewById(R.id.ivEstadoCielo31);
        ivEstado4Today = (ImageView) findViewById(R.id.ivEstadoCielo41);
        ivEstado1Tomorrow = (ImageView) findViewById(R.id.ivEstadoCielo12);
        ivEstado2Tomorrow = (ImageView) findViewById(R.id.ivEstadoCielo22);
        ivEstado3Tomorrow = (ImageView) findViewById(R.id.ivEstadoCielo32);
        ivEstado4Tomorrow = (ImageView) findViewById(R.id.ivEstadoCielo42);
        tvTemperaturaMaxToday = (TextView) findViewById(R.id.tvTemperaturaMaxToday);
        tvTemperaturaMaxTomorrow = (TextView) findViewById(R.id.tvTemperaturaMaxTomorrow);
        tvTemperaturaMinToday = (TextView) findViewById(R.id.tvTemperaturaMinToday);
        tvTemperaturaMinTomorrow = (TextView) findViewById(R.id.tvTemperaturaMinTomorrow);

        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.descargarFichero(URL, TEMPORAL, Ejercicio2_Activity.this, downloaded);
            }
        });

        Utilities.descargarFichero(URL, TEMPORAL, this, downloaded);
    }

    private String parseDate(Date day) {
        String date = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("EE");
        date = dateFormat.format(day);
        dateFormat = new SimpleDateFormat("dd");
        date += " "+dateFormat.format(day);
        return date;
    }
}
