package com.phile.yrj.tema3;

/**
 * Created by usuario on 21/11/16.
 */

public class Empleado {
    private String nombre;
    private String puesto;
    private int edad;
    private float sueldo;
    public Empleado(String nombre, String puesto, int edad, float sueldo) {
        this.setNombre(nombre);
        this.setPuesto(puesto);
        this.setEdad(edad);
        this.setSueldo(sueldo);
    }

    public Empleado() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }
}
