package com.phile.yrj.tema3;

import java.io.Serializable;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by yeray697 on 8/12/16.
 */
public class Bizi implements Serializable{
    private String id;
    private String uri;
    private String titulo;
    private String estado;
    private String bicisDisponibles;
    private String anclajesDisponibles;
    private String[] coordenadas;
    private String ultimaActualizacion;

    public String getUltimaActualizacion() {
        String mensaje = ultimaActualizacion;
        mensaje = mensaje.replace('T',' ').replace("Z","");
        return "Última actualización: " + mensaje;
    }

    public void setUltimaActualizacion(String ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public String[] getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String[] coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getAnclajesDisponibles() {
        return anclajesDisponibles;
    }

    public void setAnclajesDisponibles(String anclajesDisponibles) {
        this.anclajesDisponibles = anclajesDisponibles;
    }

    public String getBicisDisponibles() {
        return bicisDisponibles;
    }

    public void setBicisDisponibles(String bicisDisponibles) {
        this.bicisDisponibles = bicisDisponibles;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}