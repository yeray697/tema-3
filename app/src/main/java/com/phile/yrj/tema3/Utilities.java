package com.phile.yrj.tema3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by usuario on 21/11/16.
 */

public class Utilities {

    public static List<Empleado> readXML1(XmlResourceParser ficheroXML) {

        final String EMPLEADO = "empleado";
        final String NOMBRE = "nombre";
        final String PUESTO = "puesto";
        final String EDAD = "edad";
        final String SUELDO = "sueldo";

        XmlPullParser xpp = ficheroXML;
        ArrayList<Empleado> empleados = null;
        int eventType = 0;
        try {
            eventType = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        Empleado empleadoAux = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            try {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        empleados = new ArrayList();
                        break;
                    case XmlPullParser.START_TAG:
                        name = xpp.getName();
                        if (name.equals(EMPLEADO)) {
                            empleadoAux = new Empleado();
                        } else if (empleadoAux != null) {
                            if (name.equals(NOMBRE)) {
                                empleadoAux.setNombre(xpp.nextText());
                            } else if (name.equals(PUESTO)) {
                                empleadoAux.setPuesto(xpp.nextText());
                            } else if (name.equals(EDAD)) {
                                empleadoAux.setEdad(Integer.parseInt(xpp.nextText()));
                            } else if (name.equals(SUELDO)) {
                                empleadoAux.setSueldo(Float.parseFloat(xpp.nextText()));
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = xpp.getName();
                        if (name.equalsIgnoreCase(EMPLEADO) && empleadoAux != null) {
                            empleados.add(empleadoAux);
                        }
                }
                eventType = xpp.next();
            }
            catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return empleados;
    }

    public static List<TiempoDiario> readXML2(String ficheroXML) {

        final String DIA = "dia";
        final String FECHA = "fecha";
        final String ESTADO_CIELO = "estado_cielo";
        final String PERIODO = "periodo";
        final String PERIODOHORA00 = "00-06";
        final String PERIODOHORA06 = "06-12";
        final String PERIODOHORA12 = "12-18";
        final String PERIODOHORA18 = "18-24";
        final String TEMPERATURA = "temperatura";
        final String TEMPERATURA_MIN = "minima";
        final String TEMPERATURA_MAX = "maxima";

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        boolean diaValido = false;

        XmlPullParser xpp = null;
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput( new StringReader (ficheroXML) );
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        ArrayList<TiempoDiario> tiempoDiarios = null;
        int eventType = 0;
        try {
            eventType = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        TiempoDiario tiempoDiarioAux = null;
        boolean temperatura = false;

        while (eventType != XmlPullParser.END_DOCUMENT){
            try {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        tiempoDiarios = new ArrayList();
                        break;
                    case XmlPullParser.START_TAG:
                        name = xpp.getName();
                        if (name.equals(DIA)) {
                            if (xpp.getAttributeName(0).equals(FECHA)) {
                                Calendar calendar = Calendar.getInstance();
                                if (xpp.getAttributeValue(0).equals(format1.format(calendar.getTime()))){
                                    //Si hoy es ese día:
                                    tiempoDiarioAux = new TiempoDiario();
                                    tiempoDiarioAux.setDay(calendar.getTime());
                                    diaValido = true;
                                } else {
                                    calendar = Calendar.getInstance();
                                    calendar.add(Calendar.DAY_OF_MONTH,1);
                                    if (xpp.getAttributeValue(0).equals(format1.format(calendar.getTime()))){
                                        //Si es mañana
                                        tiempoDiarioAux = new TiempoDiario();
                                        tiempoDiarioAux.setDay(calendar.getTime());
                                        diaValido = true;
                                    } else
                                        diaValido = false;
                                }
                            }
                        } else if (diaValido) {
                            if (name.equals(ESTADO_CIELO)) {
                                //if (xpp.getAttributeName(0).equals(PERIODO)){
                                    if (xpp.getAttributeValue(0).equals(PERIODOHORA00)) {
                                        tiempoDiarioAux.setEstadoCielo00(xpp.nextText());
                                    } else if (xpp.getAttributeValue(0).equals(PERIODOHORA06)) {
                                        tiempoDiarioAux.setEstadoCielo06(xpp.nextText());
                                    } else if (xpp.getAttributeValue(0).equals(PERIODOHORA12)) {
                                        tiempoDiarioAux.setEstadoCielo12(xpp.nextText());
                                    } else if (xpp.getAttributeValue(0).equals(PERIODOHORA18)) {
                                        tiempoDiarioAux.setEstadoCielo18(xpp.nextText());
                                    }
                                //}
                            } else {
                                if (name.equals(TEMPERATURA)) {
                                    temperatura = true;
                                } else {
                                    if (name.equals(TEMPERATURA_MAX) && temperatura) {
                                        tiempoDiarioAux.setTemperaturaMax(Integer.parseInt(xpp.nextText()));
                                        temperatura = true;
                                    } else if(name.equals(TEMPERATURA_MIN) && temperatura) {
                                        tiempoDiarioAux.setTemperaturaMin(Integer.parseInt(xpp.nextText()));
                                        temperatura = false;
                                    }
                                }
                            }
                        }

                        break;
                    case XmlPullParser.END_TAG:
                        name = xpp.getName();
                        if (name.equalsIgnoreCase(DIA) && diaValido && tiempoDiarioAux != null) {
                            tiempoDiarios.add(tiempoDiarioAux);
                        }
                }
                eventType = xpp.next();
            }
            catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return tiempoDiarios;
    }

    public static List<Bizi> readXML3(String ficheroXML) {

        final String ESTACION = "estacion";
        final String ID = "id";
        final String URI = "uri";
        final String TITLE = "title";
        final String ESTADO = "estado";
        final String BICIS_DISP = "bicisDisponibles";
        final String ANCLAJES_DISP = "anclajesDisponibles";
        final String COORDINATES = "coordinates";
        final String LASTUPDATED = "lastUpdated";

        XmlPullParser xpp = null;
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput( new StringReader (ficheroXML) );
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        ArrayList<Bizi> bizis = null;
        int eventType = 0;
        try {
            eventType = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        Bizi biziAux = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            try {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        bizis = new ArrayList();
                        break;
                    case XmlPullParser.START_TAG:
                        name = xpp.getName();
                        if (name.equals(ESTACION)) {
                            biziAux = new Bizi();
                        } else if (name.equals(ID)) {
                            biziAux.setId(xpp.nextText());
                        }
                        else if (name.equals(URI)) {
                            biziAux.setUri(xpp.nextText());
                        }
                        else if (name.equals(TITLE)) {
                            biziAux.setTitulo(xpp.nextText());
                        } else if (name.equals(ESTADO)) {
                            biziAux.setEstado(xpp.nextText());
                        }
                        else if (name.equals(BICIS_DISP)) {
                            biziAux.setBicisDisponibles(xpp.nextText());
                        }
                        else if (name.equals(ANCLAJES_DISP)) {
                            biziAux.setAnclajesDisponibles(xpp.nextText());
                        }
                        else if (name.equals(COORDINATES)) {
                            biziAux.setCoordenadas(xpp.nextText().split(","));
                        }
                        else if (name.equals(LASTUPDATED)) {
                            biziAux.setUltimaActualizacion(xpp.nextText());
                        }

                        break;
                    case XmlPullParser.END_TAG:
                        name = xpp.getName();
                        if (name.equals(ESTACION) && biziAux != null) {
                            bizis.add(biziAux);
                        }
                }
                eventType = xpp.next();
            }
            catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bizis;
    }

    public static List<Noticia> readXML4(String ficheroXML) {

        final String ITEM = "item";
        final String TITULO = "title";
        final String DESCRIPCION = "description";
        final String FECHA = "pubDate";
        final String LINK = "link";

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");


        XmlPullParser xpp = null;
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput( new StringReader (ficheroXML) );
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        ArrayList<Noticia> noticias = null;
        int eventType = 0;
        try {
            eventType = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        Noticia noticiaAux = new Noticia();

        while (eventType != XmlPullParser.END_DOCUMENT){
            try {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        noticias = new ArrayList();
                        break;
                    case XmlPullParser.START_TAG:
                        name = xpp.getName();
                        if (name.equals(ITEM)) {
                            noticiaAux = new Noticia();
                        } else if (name.equals(TITULO)) {
                            noticiaAux.setTitulo(xpp.nextText());
                        } else if (name.equals(DESCRIPCION)) {
                            noticiaAux.setDescripcion(xpp.nextText());
                        } else if (name.equals(FECHA)) {
                            noticiaAux.setPubDate(xpp.nextText());
                        } else if (name.equals(LINK)) {
                            noticiaAux.setLink(xpp.nextText());
                        }

                        break;
                    case XmlPullParser.END_TAG:
                        name = xpp.getName();
                        if (name.equalsIgnoreCase(ITEM)) {
                            noticias.add(noticiaAux);
                        }
                }
                eventType = xpp.next();
            }
            catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        noticiaAux.setPubDate(noticiaAux.getPubDate());
        return noticias;
    }

    public interface Downloaded {
        void onDownloadedResult();
        void onFailureResult();
    }

    public static void descargarFichero(String rss, String temporal, final Context context, Downloaded IDownloaded) {
        final ProgressDialog progreso = new ProgressDialog(context);
        final Downloaded downloaded = IDownloaded;
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        RestClient.get(rss, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                downloaded.onFailureResult();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                downloaded.onDownloadedResult();
                progreso.dismiss();
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }

    public static String readExternal(String fileName, String codification){
        String result = "";
        if (canReadExternal()){
            File file, externalCardName;
            externalCardName = Environment.getExternalStorageDirectory();
            file = new File(externalCardName.getAbsolutePath(), fileName);
            result = read(file, codification);
        }
        return result;
    }

    public static boolean canReadExternal(){
        boolean canRead = false;
//Comprobamos el estado de la memoria externa (tarjeta SD)
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)
                || state.equals(Environment.MEDIA_MOUNTED))
            canRead = true;
        return canRead;
    }

    private static String read(File file, String codification){
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        StringBuilder result = new StringBuilder();
        int n;
        try {
            fileInputStream = new FileInputStream(file);
            inputStreamReader = new InputStreamReader(fileInputStream, codification);
            bufferedReader = new BufferedReader(inputStreamReader);
            while ((n = bufferedReader.read()) != -1)
                result.append((char) n);
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return result.toString();
    }
}