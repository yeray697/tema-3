package com.phile.yrj.tema3;

import java.util.Date;

/**
 * Created by usuario on 22/11/16.
 */

public class TiempoDiario {
    private Date day;
    private String estadoCielo00;
    private String estadoCielo06;
    private String estadoCielo12;
    private String estadoCielo18;
    private int temperaturaMin;
    private int temperaturaMax;

    final static String URL1 = "http://www.aemet.es/imagenes/png/estado_cielo/";
    final static String URL2 = ".png";

    public TiempoDiario(){

    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }


    public int getTemperaturaMin() {
        return temperaturaMin;
    }

    public void setTemperaturaMin(int temperaturaMin) {
        this.temperaturaMin = temperaturaMin;
    }

    public int getTemperaturaMax() {
        return temperaturaMax;
    }

    public void setTemperaturaMax(int temperaturaMax) {
        this.temperaturaMax = temperaturaMax;
    }

    public String getEstadoCielo00() {
        String estado = "null";
        if (!estadoCielo00.equals(""))
            estado = URL1+estadoCielo00+URL2;
        return estado;
    }

    public void setEstadoCielo00(String estadoCielo00) {
        this.estadoCielo00 = estadoCielo00;
    }

    public String getEstadoCielo06() {
        String estado = "null";
        if (!estadoCielo06.equals(""))
            estado = URL1+estadoCielo06+URL2;
        return estado;
    }

    public void setEstadoCielo06(String estadoCielo06) {
        this.estadoCielo06 = estadoCielo06;
    }

    public String getEstadoCielo12() {
        String estado = "null";
        if (!estadoCielo12.equals(""))
            estado = URL1+estadoCielo12+URL2;
        return estado;
    }

    public void setEstadoCielo12(String estadoCielo12) {
        this.estadoCielo12 = estadoCielo12;
    }

    public String getEstadoCielo18() {
        String estado = "null";
        if (!estadoCielo18.equals(""))
            estado = URL1+estadoCielo18+URL2;
        return estado;
    }

    public void setEstadoCielo18(String estadoCielo18) {
        this.estadoCielo18 = estadoCielo18;
    }
}
