#### Todas las activities pueden cambiar de orientación de pantalla.
#### En caso de no haber internet muestra un error, pero la aplicación no crashea.
#### Para todas las imágenes que aparecen he utilizado Picasso.
.
# 1. Crear un documento XML que contenga una serie de datos sobre los empleados de una empresa: su nombre, puesto, edad y sueldo. Realizar una aplicación que calcule la edad media, el sueldo mínimo y el máximo de todos los empleados almacenados en dicho documento XML.
###### Muestra una serie de textviews que muestra la información.
###### El documento está creado desde Android Studio

# 2. Crear una aplicación que muestre en pantalla la predicción meteorológica (estado del cielo y temperaturas mínima y máxima) para hoy y mañana en Málaga. La información del tiempo se puede obtener del documento XML para Málaga en la Aemet.
###### Muestra una tabla igual que la que se muestra la web.
###### Ya que horizontalmente no cabía, está puesta sobre un HorizontalScrollView

# 3. En la web ESTACIONES BIZI ZARAGOZA se puede obtener la relación de estaciones de bicicletas de la ciudad de Zaragoza con información sobre el estado de la estación, nº de anclajes y nº de bicicletas disponibles, entre otros datos.
# Crear una aplicación que obtenga el listado de estaciones Bizi (actualizado cada 2 minutos) en formato xml y muestre una lista con todas las estaciones. Cuando se pulse en una de ellas, se mostrará su estado, las bicis disponibles, los anclajes y la fecha y hora de la última modificación. Además, se podrá ver el mapa de esa estación.
###### Hecho exactamente como pedías.

# 4. Buscar varios sitios web que faciliten noticias en formato RSS (El País, El Mundo, Linux Magazine, PCWorld,  . . . ) y crear una aplicación para descargar sus últimas noticias. Se mostrarán los diferentes sitios en una lista; cuando se pulse en uno, aparecerá una nueva lista con los titulares de las noticias en ese sitio y, cuando se elija uno de esos titulares, se mostrará información ampliada de dicha noticia en el navegador en una nueva ventana.
###### Hecho exactamente como pedías.